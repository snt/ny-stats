#!/usr/bin/env ruby
# frozen_string_literal: true

# Calculate statistics about the New York Pizza orders
require 'yaml'
require 'time'
require 'erb'
require 'fileutils'
require 'pearson'
require 'json'

# Add statistics functions to Enumerable
# Original by tolitius, https://stackoverflow.com/a/7749613/
module Enumerable
  def sum
    inject(0) { |accum, i| accum + i }
  end

  def mean
    sum / length.to_f
  end

  def var
    m = mean
    sum = inject(0) { |accum, i| accum + (i - m)**2 }
    sum / (length - 1).to_f
  end

  def stdev
    Math.sqrt(var)
  end
end

# Add translation to String
class String
  def to_dutch
    case self
    when 'delay'  then 'vertraging'
    when 'notice' then 'vooraankondiging'
    else self
    end
  end
end

# Single statistic
class Stat
  attr_reader :data, :date, :besteltijd, :streeftijd, :levertijd, :aantal,
              :comment

  def initialize(data)
    @data       = data
    @date       = data['date']
    @besteltijd = calc_time data['besteltijd']
    @streeftijd = calc_time data['streeftijd']
    @levertijd  = calc_time data['levertijd']
    @aantal     = data['aantal']
    @comment    = data['comment']
  end

  # Calculate the delay for an entry in minutes
  # Returns nil for invalid values
  def delay
    (@levertijd - @streeftijd) / 60
  rescue StandardError
    nil
  end

  # Calculate the advance notice of the order in minutes
  # Returns nil for invalid values
  def notice
    (@streeftijd - @besteltijd) / 60
  rescue StandardError
    nil
  end

  private

  # Create a full Time object from a timestamp (in seconds)
  def calc_time(time)
    @date.to_time + time
  end
end

# Statistics page renderer
class NYStats
  INDEX_P = 'index.html'
  INDEX_T = 'index.erb'
  STATS_JSON = 'stats.json'

  # Intialise by loading data and creating output folder
  def initialize(data_file, public_folder)
    @data     = load_data data_file
    @folder   = public_folder
    @template = File.read INDEX_T
    FileUtils.mkdir_p @folder
  end

  # Load data from file
  def load_data(file)
    data = Psych.safe_load(File.read(file), permitted_classes: [Date])
    data.map! { |d| Stat.new d }
  end

  # Create an array of stats for a list
  def stats_array(list)
    list.map!(&:to_f)
    stats = [
      list.min,
      list.mean,
      list.max,
      list.stdev
    ]
    stats.map! { |s| s.round(1) }
  end

  # Create a string of the stats for a list.
  def stats(list)
    stats_array(list).join('/')
  end

  # Create a hash containing metric by date
  def by_date(metric)
    @data.each_with_object({}) { |d, h| h[d.date] = d.send metric }
  end

  # Calculate delays
  def delays
    @data.map(&:delay).compact
  end

  # Calculate notices
  def notices
    @data.map(&:notice).compact
  end

  # Calculate delay stats
  def delay_stats
    stats delays
  end

  # Calculate aantallen
  def aantallen
    @data.map(&:aantal).compact
  end

  # Calculate number of pizzas stat
  def aantal_stats
    stats aantallen
  end

  # Render template
  def render
    ERB.new(@template).result(binding)
  end

  # Write HTML
  def write_html
    File.write File.join(@folder, INDEX_P), render
  end

  # Write JSON for Plot.ly
  def write_json(last = 5)
    data = stats_hash.map do |k, d|
      {
        name: k.to_s.to_dutch,
        x: d.keys.last(last),
        y: d.values.last(last),
        type: :bar
      }
    end

    plot = { data: data, layout: { yaxis: { title: 'minutes' } } }

    File.write File.join(@folder, STATS_JSON), plot.to_json
  end

  # Create a hash of statistics for correlation calculations.
  def stats_hash
    keys = %i[aantal delay notice]
    keys.each_with_object({}) { |k, h| h[k] = by_date k }
  end

  # Calculate the pearson correlation coefficient between two entities
  def coefficient(one, two)
    Pearson.coefficient stats_hash, one, two
  end
end

# Load the dataset
ny = NYStats.new('newyork.yaml', 'public')

# Write page(s) and data
ny.write_html
ny.write_json

# Show statistics
puts "Delivery latency min/avg/max/std-dev: #{ny.delay_stats} min"
puts "Correlation between number of pizzas and delay: #{ny.coefficient(:aantal, :delay).round 3}"
puts "Correlation between advance notice and delay: #{ny.coefficient(:notice, :delay).round 3}"
